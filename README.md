# PdpdOcrWeb

#### 介绍
基于百度飞桨paddlepaddle的web在线ocr识别服务端

#### 软件架构
基于百度飞桨paddle-ocr


#### 安装与运行

直接运行即可
`python main.py`

如提示缺少module请自行安装，例如`pip install paddlepaddle`